const pgPromise = require('pg-promise');
const R = require('ramda');
const request = require('request-promise');

// Limit the amount of debugging of SQL expressions
const trimLogsSize: number = 200;

// Database interface
interface DBOptions {
  host: string;
  database: string;
  user?: string;
  password?: string;
  port?: number;
}

// Actual database options
const options: DBOptions = {
  user: 'virtualbox',
  password: 'virtualbox',
  host: 'localhost',
  database: 'lovelystay_test',
  port: 5432
};

// Initialization
console.info(
  'Connecting to the database:',
  `${options.user}@${options.host}:${options.port}/${options.database}`
);

const pgpDefaultConfig = {
  promiseLib: require('bluebird'),
  // Log all querys
  query(query) {
    console.log('[SQL   ]', R.take(trimLogsSize, query.query));
  },
  // On error, please show me the SQL
  error(err, e) {
    if (e.query) {
      console.error('[SQL   ]', R.take(trimLogsSize, e.query), err);
    }
  }
};

interface GithubUsers {
  id: number;
  login: string;
}

interface LocationCount {
  location: string;
  count: number;
}

const pgp = pgPromise(pgpDefaultConfig);
const db = pgp(options);

/**
 * Initializes the table with the required fields.
 */

function initTable() {
  return db.none(
    'CREATE TABLE IF NOT EXISTS github_users (' +
      'id BIGSERIAL PRIMARY KEY NOT NULL, ' +
      'login TEXT UNIQUE, ' +
      'name TEXT, ' +
      'company TEXT, ' +
      'location TEXT)'
  );
}

/**
 * Requests the user from github by the login name and inserts it on the db.
 * If the user already exists, updates the row.
 *
 * @param loginName // user login name
 */

function insertUser(loginName: string) {
  return request({
    uri: 'https://api.github.com/users/' + loginName,
    headers: {
      'User-Agent': 'Request-Promise'
    },
    json: true
  })
    .then((data: GithubUsers) => {
      return db.one(
        'INSERT INTO github_users (id, login, name, company, location) ' +
          'VALUES ($[id], $[login], $[name], $[company], $[location]) ' +
          'ON CONFLICT (id) DO UPDATE SET ' +
          'login = EXCLUDED.login, ' +
          'name = EXCLUDED.name, ' +
          'company = EXCLUDED.company, ' +
          'location = EXCLUDED.location ' +
          'RETURNING id',
        data
      );
    })
    .then(({ id }) => console.log(id));
}

/**
 * Requests all the users followed by the provided user and iterates
 * through insertUser
 *
 * @param loginName // user login name
 */
function insertUserFollowed(loginName: string) {
  return request({
    uri: 'https://api.github.com/users/' + loginName + '/following',
    headers: {
      'User-Agent': 'Request-Promise'
    },
    json: true
  }).then((response: Array<GithubUsers>) => {
    return Promise.all(
      response.map(user => {
        return insertUser(user.login);
      })
    );
  });
}

/**
 * Displays the names of the users that match the provided location
 *
 * @param locationName // location name
 */
function listByLocation(locationName: string) {
  return db
    .any({
      text:
        'SELECT * FROM github_users ' +
        'WHERE UPPER(github_users.location) ' +
        "LIKE UPPER($1 || '%')",
      values: [locationName]
    })
    .then((result: Array<GithubUsers>) =>
      result.forEach(user => console.log(user.login))
    );
}

/**
 * Lists the count of users that have the same location in common
 */
function usersByLocation() {
  return db
    .any(
      'SELECT location, COUNT (location) FROM github_users ' +
        'WHERE location IS NOT NULL GROUP BY location'
    )
    .then((result: Array<LocationCount>) =>
      result.forEach(value => console.log(value.location + ': ' + value.count))
    );
}

if (process.argv.length < 3) {
  console.info(
    'Syntax:\n' +
      'npm test option <value>\n\n' +
      'Options:\n' +
      'add - Adds/updates the user to the database\n' +
      'followed - Adds all the users that the provided user is following to ' +
      'the database\n' +
      'listbylocation - Lists all the users that match the provided ' +
      'location\n' +
      'usersbylocation - Shows the number of users that share a common location'
  );

  process.exit(0);
} else {
  const option = process.argv[2];
  const target = process.argv[3];

  const argOptions = {
    add: value => {
      return insertUser(value);
    },
    followed: value => {
      return insertUserFollowed(value);
    },
    listbylocation: value => {
      return listByLocation(value);
    },
    usersbylocation: () => {
      return usersByLocation();
    }
  };

  initTable()
    .then(() => argOptions[option](target))
    .catch(() => console.log('\nInvalid argument provided!'))
    .then(() => process.exit(0));
}
